<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\AbsenController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); });
Route::get('/inventaris', function () { return view('inventaris'); });
Route::get('/simpan-inventaris', function () { return view('simpan-inventaris'); });
Route::get('/cetak-inventaris', function () { return view('cetak-inventaris'); });
Route::get('/jadwal-inventaris', function () { return view('jadwal-inventaris'); });

Route::get('inventaris', [BarangController::class, 'index'])->name('inventaris');
Route::get('create-inventaris', [BarangController::class, 'create'])->name('create-inventaris');
Route::post('simpan-inventaris', [BarangController::class, 'store'])->name('simpan-inventaris');
Route::get('edit-inventaris/{id}', [BarangController::class, 'edit'])->name('edit');
Route::get('cetak-inventaris', [BarangController::class, 'semua'])->name('index');
Route::post('update-inventaris/{id}', [BarangController::class, 'update'])->name('update');
Route::get('delete-inventaris/{id}', [BarangController::class, 'destroy'])->name('delete');

Route::get('jadwal-inventaris', [AbsenController::class, 'index'])->name('jadwal-inventaris');
Route::get('create-absen', [AbsenController::class, 'create'])->name('create-absen');
Route::post('simpan-absen', [AbsenController::class, 'store'])->name('simpan-absen');
Route::get('edit-absen/{id}', [AbsenController::class, 'edit'])->name('edit-absen');
Route::post('update-absen/{id}', [AbsenController::class, 'update'])->name('update-absen');
Route::get('delete-absen/{id}', [AbsenController::class, 'destroy'])->name('delete-absen');

Route::get('export-pegawai', [AbsenController::class, 'absenexport'])->name('export-pegawai');
Route::get('inventaris-barang', [BarangController::class, 'barangexport'])->name('inventaris-barang');

Route::get('/filter-data', [BarangController::class, 'search'])->name('filter-data');
Route::get('/filter-data/{tanggaldari}', [BarangController::class, 'findsearch'])->name('filter-data-sort');

// Route::get('filter-data', [BarangController::class, 'viewsearch'])->name('filter-data');
// Route::get('filter-data/{nama_barang}', [BarangController::class, 'search'])->name('filter-data-all');