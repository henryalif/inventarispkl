<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Web Inventaris</title>

    <!-- Custom fonts for this template-->
    <script src="https://kit.fontawesome.com/68ff74069c.js" crossorigin="anonymous"></script>
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('template/css/sb-admin-2.css')}}">
    <link rel="stylesheet" href="{{ asset('template/css/sb-admin-2.min.css')}}">
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">PT. Solusi Intek</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="/">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Menu
            </div>
            
            <li class="nav-item">
                <a class="nav-link" href="/jadwal-inventaris">
                    <i class="fas fa-calendar-alt"></i>
                    <span>Jadwal</span></a>
            </li>
            <!-- Nav Item - Charts -->
            <li class="nav-item">
                <a class="nav-link" href="/inventaris">
                    <i class="fas fa-warehouse"></i>
                    <span>Inventaris</span></a>
            </li>

            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="/cetak-inventaris">
                    <i class="fas fa-print"></i>
                    <span>Cetak Inventaris</span></a>
            </li>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800 mb-4">Halaman Inventaris</h1>

                    <!-- DataTales Example -->
                    <div class="card border-bottom-info shadow mb-4">
                        <div class="card-header">
                        </div>

                        <div class="card-body">
                            <form action="{{ url('simpan-inventaris') }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Nama barang</label>
                                    <input name="nama_barang" id:"nama_barang" type="text" class="form-control"
                                        placeholder="Isi Nama Barang" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Jumlah</label>
                                    <input name="jumlah" id:"jumlah" type="number" class="form-control"
                                        placeholder="Isi Jumlah Barang" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Lantai</label>
                                    <select name="lantai" id:"lantai" class="form-control" required>
                                        <option selected> -- Pilih --</option>
                                        <option value="Lantai 1">1</option>
                                        <option value="Lantai 2">2</option>
                                        <option value="Lantai 3">3</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Keterangan</label>
                                    <textarea placeholder="Isi Keterangan" name="keterangan" id:"Keterangan" class="form-control" aria-label="With textarea" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Tanggal</label>
                                    <input name="tanggal" id:"tanggal" type="date" class="form-control"
                                        required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success float-right">Tambahkan</button>
                                    <a href="{{ route('inventaris') }}" class="btn btn-secondary float-right" style="margin-right: 1rem">Kembali</a>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; PT. Solusi Intek Indonesia</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->


    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
