<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absen extends Model
{
    use HasFactory;
    
    protected $table = "absen";
    protected $primaryKey = "id";
    protected $fillable = [
       'id',
       'nama_pegawai',
       'nama_barang',
       'qty',
       'tanggal',
       'lantai',
       'keterangan',
   ];

    /**
    * Set nama barang
    */
    // public function setBarangAttribute($value)
    // {
    //     $this->attributes('nama_barang') = json_encode($value);
    // }

    /**
     * Get nama barang
     */
    // public function getBarangAttribute($value)
    // {
    //     return $this->attributes('nama_barang') = json_decode($value);
    // }
}
