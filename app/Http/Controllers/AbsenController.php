<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Absen;

use App\Exports\AbsenExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class AbsenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abController = Absen::all();
        return view('jadwal-inventaris',compact('abController'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create-absen');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Absen::create([
            'nama_pegawai' => $request->nama_pegawai,
            'nama_barang' => $request->nama_barang,
            'qty' => $request->qty,
            'tanggal' => $request->tanggal,
            'lantai' => $request->lantai,
            'keterangan' => $request->keterangan,
        ]);

        return redirect('jadwal-inventaris');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function absenexport()
    {
        return Excel::download(new AbsenExport, 'Absen-Pegawai.xlsx');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editAb = Absen::findorfail($id);
        return view('edit-absen', compact('editAb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editAb = Absen::findorfail($id);
        $editAb->update($request->all());

        return redirect('jadwal-inventaris');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delAb = Absen::findorfail($id);
        $delAb->delete();
        return back();
    }
}
