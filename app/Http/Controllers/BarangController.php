<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Barang;

use App\Exports\BarangExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invenController = Barang::all();
        return view('inventaris',compact('invenController'));
    }

    public function semua()
    {
        $invenController = Barang::all();
        return view('cetak-inventaris',compact('invenController'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create-inventaris');
    }

    public function findsearch()
    {
        // $barang = $request->barang;
        // $invenController = Barang::where('nama_barang','like',"%".$barang."%")->orderBy('nama_barang', 'asc')->get();
        
        return view('cetak-inventaris');
    }

    public function search(Request $request)
    {
        // return view('cetak-inventaris', ['nama_barang' => $data]);
        $barang = $request->barang;
        $invenController = Barang::where('nama_barang','like',"%".$barang."%")->orderBy('nama_barang', 'asc')->get();
        
        return view('find-data', compact('invenController'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Barang::create([
            'nama_barang' => $request->nama_barang,
            'jumlah' => $request->jumlah,
            'lantai' => $request->lantai,
            'keterangan' => $request->keterangan,
            'tanggal' => $request->tanggal,
        ]);

        return redirect('inventaris');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function barangexport()
    {
        return Excel::download(new BarangExport, 'Inventaris-Barang.xlsx');
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editBrg = Barang::findorfail($id);
        return view('edit-inventaris', compact('editBrg'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editBrg = Barang::findorfail($id);
        $editBrg->update($request->all());

        return redirect('inventaris');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delBrg = Barang::findorfail($id);
        $delBrg->delete();
        return back();
    }
}
